package service;



import dao.*;
import entity.*;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mindrot.jbcrypt.BCrypt;
import util.Resource;

import javax.inject.Inject;

import static org.junit.Assert.*;

@RunWith(Arquillian.class)
public class OrderServiceImplTest {
    @Deployment
    public static WebArchive createDeployment() {
        return ShrinkWrap.create(WebArchive.class, "test.war")
                .addPackage(EndUser.class.getPackage())
                .addPackage(EndUserDAOImpl.class.getPackage())
                .addPackage(OrderDAOImpl.class.getPackage())
                .addPackage(OrderEn.class.getPackage())
                .addPackage(OrderServiceImpl.class.getPackage())
                .addClass(Resource.class)
                .addClass(BCrypt.class)
                .addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
                .addAsResource("import.sql")
                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
    }


    @Inject
    private OrderService orderService;

    private String username = "user1";

    @Test
    @InSequence(1)
    public void createOrder_correct() {
        OrderEn o = new OrderEn();
        o.setActive(true);
        o.setAddress("Praha");
        o.setDescription("blablbala");
        o.setName("Skvela nabidka");
        assertEquals(0, orderService.createNewOrder(o, username));
    }

    @Test
    @InSequence(2)
    public void createOrder_fail() {
        OrderEn o = new OrderEn();
        o.setActive(true);
        o.setAddress("");
        o.setDescription("blablbala");
        o.setName("Skvela nabidka");
        assertEquals(1, orderService.createNewOrder(o, username));
    }

    @Test
    @InSequence(3)
    public void createOrder_fail2() {
        OrderEn o = new OrderEn();
        o.setActive(true);
        o.setAddress("asdadsad");
        o.setDescription("blablbala");
        o.setName("Skvela nabidka");
        assertEquals(3, orderService.createNewOrder(o, "Martin"));
    }

}
