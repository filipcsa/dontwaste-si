package service;

import dao.AccountDAOImpl;
import dao.EndUserDAOImpl;
import entity.Account;
import entity.EndUser;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mindrot.jbcrypt.BCrypt;
import util.Resource;

import javax.inject.Inject;

import static org.junit.Assert.*;

@RunWith(Arquillian.class)
public class SignUpServiceImplTest {
    @Deployment
    public static WebArchive createDeployment() {
        return ShrinkWrap.create(WebArchive.class, "test.war")
                .addPackage(EndUser.class.getPackage())
                .addPackage(EndUserDAOImpl.class.getPackage())
                .addPackage(SignUpServiceImpl.class.getPackage())
                .addPackage(Account.class.getPackage())
                .addPackage(AccountDAOImpl.class.getPackage())
                .addPackage(LoginServiceImpl.class.getPackage())
                .addClass(Resource.class)
                .addClass(BCrypt.class)
                .addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
    }

    @Inject
    private SignUpService signUpService;

    @Inject
    private LoginService loginService;

    private String username = "testuser";
    private String email = "test@test.cz";
    private String fname = "Test";
    private String lname = "User";
    private String pass = "password";

    @Test
    @InSequence(1)
    public void signUpUser_correct() {
        EndUser user = new EndUser();
        user.setUsername(username);
        user.setEmail(email);
        user.setFirtsname(fname);
        user.setLastname(lname);
        assertEquals(0, signUpService.signUpUser(user, pass));
    }

    @Test
    @InSequence(2)
    public void SignUpUser_duplicateUsername(){
        EndUser user = new EndUser();
        user.setUsername(username);
        user.setEmail(email);
        user.setFirtsname(fname);
        user.setLastname(lname);
        assertEquals(1, signUpService.signUpUser(user, pass));
    }

    @Test
    @InSequence(3)
    public void tryLoginForNewUser() {
        assertTrue(loginService.validateLogin(
                "testuser", "password"));
    }
}
