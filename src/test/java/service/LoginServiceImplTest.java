package service;

import dao.AccountDAOImpl;
import entity.Account;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mindrot.jbcrypt.BCrypt;
import util.Resource;

import javax.inject.Inject;

import static org.junit.Assert.*;

@RunWith(Arquillian.class)
public class LoginServiceImplTest {
    @Deployment
    public static WebArchive createDeployment() {
        return ShrinkWrap.create(WebArchive.class, "test.war")
                .addPackage(Account.class.getPackage())
                .addPackage(AccountDAOImpl.class.getPackage())
                .addPackage(LoginServiceImpl.class.getPackage())
                .addClass(Resource.class)
                .addClass(BCrypt.class)
                .addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
                .addAsResource("import.sql")
                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
    }

    @Inject
    private LoginService loginService;

    @Test
    public void checkPassword() {
        String pass = "Veslo123+";
        String hash = "$2a$04$8RxhkE3VGLXC.EvzHvAEBOCmzRun93We/4JjkzS.fCC8daXU7Z4IO";
        assertTrue(loginService.checkPassword(pass, hash));
    }

    @Test
    public void validateLogin(){
        String username = "Martin";
        String fpass = "neniretard";
        assertFalse(loginService.validateLogin(username, fpass));
        String rpass = "retard";
        assertTrue(loginService.validateLogin(username, rpass));
    }
}