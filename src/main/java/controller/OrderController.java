package controller;


import entity.OrderEn;
import service.OrderService;
import util.SessionUtil;

import javax.annotation.ManagedBean;
import javax.annotation.PostConstruct;
import javax.enterprise.inject.Model;
import javax.enterprise.inject.Produces;
import javax.faces.application.FacesMessage;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

@Model
@ManagedBean
@SessionScoped
public class OrderController implements Serializable {


    // All active orders
    private List<OrderEn> otherOrders;
    // All my active orders
    private List<OrderEn> myOrders;

    // Update / Delete
    private OrderEn selectedOrder;

    @Produces
    @Named
    private OrderEn newOrder;

    @Inject
    private FacesContext facesContext;

    @Inject
    private OrderService orderService;

    private String username;

    /* Following methods are arranging CRUD operations*/

    // CREATE
    public void createOrder() {
        int createNewOrderStatus = orderService.createNewOrder(newOrder, username);
        if(createNewOrderStatus == 1) {
            FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_INFO, "Name, address or " +
                    "description is wrong", "Some constraints are too short");
            facesContext.addMessage(null, m);
        } else if (createNewOrderStatus == 2) {
            FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_INFO, "Your user account is unable" +
                    " to create order"
                    , "You are not logged as a user with permission to add order");
            facesContext.addMessage(null, m);
        }
        else {
            FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_INFO, "Order successfully created"
                    , "Order is OK");
            facesContext.addMessage(null, m);
        }
        this.initOrders();
    }
    // READ
    @PostConstruct
    public void initOrders() {
        username = SessionUtil.getUsername();
        newOrder = new OrderEn();
        List<OrderEn> oor = orderService.loadOtherOrders(username);
        if (oor != null) {
            otherOrders = oor;
        }
        List<OrderEn> mor = orderService.loadMyOrders(username);
        if (mor != null) {
            myOrders = mor;
        }

    }
    // UPDATE
    public void deactivateOrder() {
        orderService.deactivateOrder(this.selectedOrder);
        this.initOrders();
    }
    // DELETE
    public void deleteOrder() {
        orderService.deleteOrder(this.selectedOrder);
        this.initOrders();
    }

    public void setSelectedOrder(OrderEn selectedOrder) {
        this.selectedOrder = selectedOrder;
    }

    public OrderEn getSelectedOrder() {
        return this.selectedOrder;
    }

    public void setNewOrder(OrderEn newOrder) {
        this.newOrder = newOrder;
    }

    public OrderEn getNewOrder() {
        return this.newOrder;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List<OrderEn> getOtherOrders() {
        return otherOrders;
    }

    public void setOtherOrders(List<OrderEn> otherOrders) {
        this.otherOrders = otherOrders;
    }

    public List<OrderEn> getMyOrders() {
        return myOrders;
    }

    public void setMyOrders(List<OrderEn> myOrders) {
        this.myOrders = myOrders;
    }
}
