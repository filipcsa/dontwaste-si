package controller;

import service.LoginService;

import javax.annotation.ManagedBean;
import javax.enterprise.inject.Model;
import javax.faces.application.FacesMessage;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.Serializable;
import java.util.logging.Logger;

@Model
@ManagedBean
@SessionScoped
public class LoginController implements Serializable {

    private String username;
    private String password;

    @Inject
    private LoginService loginService;

    @Inject
    private FacesContext facesContext;

    @Inject
    private Logger logger;

    public String validateUserLogin(){
        if (loginService.validateLogin(username, password)) {
            HttpSession session = (HttpSession) facesContext
                    .getExternalContext().getSession(false);
            session.setAttribute("username", username);
            return "success";
        }
        FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_INFO, "Incorrect credentials!", "Incorrect credentials!");
        facesContext.addMessage(null, m);
        return "failure";
    }

    public String logout(){
        HttpSession session = (HttpSession) facesContext
                .getExternalContext().getSession(false);
        String username = session.getAttribute("username").toString();
        logger.info("Logging out user: " + username);
        session.invalidate();
        return "logout";
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
