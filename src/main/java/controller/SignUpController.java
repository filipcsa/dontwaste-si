package controller;

import entity.EndUser;
import service.SignUpService;

import javax.annotation.PostConstruct;
import javax.enterprise.inject.Model;
import javax.enterprise.inject.Produces;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

@Model
public class SignUpController implements Serializable {

    @Inject
    private SignUpService signUpService;

    @Inject
    private FacesContext facesContext;

    @Produces
    @Named
    private EndUser user;

    private String password;

    @PostConstruct
    public void initNewEndUser(){
        user = new EndUser();
    }

    //TODO dat uzivateli vic infa na ui
    public String signUpUser(){
        if (signUpService.signUpUser(user, password) == 0) {
            FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_INFO, "Sign up successful",
                    "Sign up successful");
            facesContext.addMessage(null, m);
            return "success";
        }
        else{
            FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_INFO, "Either the username " +
                    "is taken or the email is wrong", "Some constraints violated");
            facesContext.addMessage(null, m);
            return "failure";
        }
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


}
