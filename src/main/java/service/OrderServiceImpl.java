package service;

import dao.OrderDAOImpl;
import dao.EndUserDAOImpl;
import entity.EndUser;
import entity.OrderEn;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import java.util.List;
import java.util.logging.Logger;

@Stateless
public class OrderServiceImpl implements OrderService {

    @Inject
    OrderDAOImpl orderDAO;

    @Inject
    EndUserDAOImpl endUserDAO;

    @Inject
    Logger logger;

    @Override
    public List<OrderEn> loadMyOrders(String username) {
        List<OrderEn> orders;

        try {
            orders = orderDAO.findAllActiveMyOrders(username);
        } catch (NoResultException e) {
            logger.info("No my orders found");
            return null;
        }

        logger.info(" My orders successfully loaded");

        return orders;
    }

    @Override
    public boolean deleteOrder(OrderEn o) {
        orderDAO.delete(o.getId());
        return true;
    }

    @Override
    public boolean deactivateOrder(OrderEn o) {
        o.setActive(false);
        try {
            orderDAO.update(o);
            logger.info("Entity: Order, ID: " + o.getId() + " is deactivated");
        }
        catch (NoResultException e) {
            logger.info("Entity: Order, ID: " + o.getId() + " failed to deactivate");
            return false;
        }
        return true;
    }

    @Override
    public int createNewOrder(OrderEn o, String username) {
        EndUser u;
        try {
            u = endUserDAO.findEndUserByUsername(username);
        }
        catch (NoResultException e) {
            logger.info("No EndUser with username " + username + " found");
            return 2;
        }
        o.setEndUser(u);
        o.setActive(true);
        orderDAO.save(o);
        logger.info("New order successfully created");
        return 0;
    }


    @Override
    public List<OrderEn> loadOtherOrders(String username) {

        List<OrderEn> orders;

        try{
            orders = orderDAO.findAllActiveOtherOrders(username);
        }catch (NoResultException e){
            logger.info("No other orders found");
            return null;
        }

        logger.info(" Other orders successfully loaded");
        return orders;

    }
}
