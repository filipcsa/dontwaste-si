package service;

import entity.EndUser;

public interface SignUpService {

    //TODO checkovat, jakej constraint se podelal
    /**
     *
     * @param user
     * @param password
     * @return 0 on success
     *         1 on failure
     */
    int signUpUser(EndUser user, String password);
}
