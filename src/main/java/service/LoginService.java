package service;

public interface LoginService {

    boolean validateLogin(String username, String password);

    boolean checkPassword(String plainPassword, String hashedPassword);
}
