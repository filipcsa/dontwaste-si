package service;


import entity.OrderEn;
import java.util.*;

public interface OrderService {

    List<OrderEn> loadOtherOrders(String username);
    List<OrderEn> loadMyOrders(String username);
    int createNewOrder(OrderEn e, String username);
    boolean deleteOrder(OrderEn e);
    boolean deactivateOrder(OrderEn e);

}
