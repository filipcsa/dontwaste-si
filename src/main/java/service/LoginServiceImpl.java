package service;

import dao.AccountDAOImpl;
import entity.Account;
import org.mindrot.jbcrypt.BCrypt;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import java.util.logging.Logger;

@Stateless
public class LoginServiceImpl implements LoginService{

    @Inject
    AccountDAOImpl accountDAO;

    @Inject
    Logger logger;

    @Override
    public boolean validateLogin(String username, String password) {
        Account account;
        try{
            account = accountDAO.findAccountByUsername(username);
        }catch (NoResultException e){
            logger.info("No account with username " + username);
            return false;
        }
        String hashed = account.getPassword();
        if(checkPassword(password, hashed)) {
            logger.info(username + " entered correct password");
            return true;
        }
        logger.info("Incorrect credentials");
        return false;
    }

    @Override
    public boolean checkPassword(String plainPassword, String hashedPassword) {
        return BCrypt.checkpw(plainPassword, hashedPassword);
    }
}
