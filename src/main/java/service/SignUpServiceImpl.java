package service;

import dao.EndUserDAOImpl;
import entity.EndUser;
import org.mindrot.jbcrypt.BCrypt;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.PersistenceException;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.logging.Logger;

@Stateless
public class SignUpServiceImpl implements SignUpService {

    @Inject
    private EndUserDAOImpl endUserDAO;

    @Inject
    private Logger logger;

    @Override
    public int signUpUser(EndUser user, String password) {
        user.setPassword(hashPassword(password));
        logger.info("Password for new user hashed");

        try{
            endUserDAO.save(user);
        }
        catch (PersistenceException e){
            logger.info("Constraint violated while creating" +
                    " new user. Duplicate username or wrong email.");
            return 1;
        }
        return 0;
    }

    private String hashPassword(String password){
        String hashedPsw = BCrypt.hashpw(password, BCrypt.gensalt());
        return hashedPsw;
    }

}
