package entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.HashSet;
import java.util.Set;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity
@NamedQueries({
        @NamedQuery(name = "findEndUserByUsername",
                query = "select e from EndUser e where e.username = :username")
})
public class EndUser extends Account implements Serializable {
    
    @NotNull
    private String firtsname;

    @NotNull
    private String lastname;

    @OneToMany(mappedBy="endUser")
    private Set<OrderEn> orders;

    @NotNull
    @Pattern(regexp = "^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}$", message = "must contain valid email address")
    private String email;

    public Set<OrderEn> getOrders() {
        return this.orders;
    }

    public void setOrders(Set<OrderEn> orders) {
        this.orders = orders;
    }

    public String getFirtsname() {
        return firtsname;
    }

    public void setFirtsname(String firtsname) {
        this.firtsname = firtsname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
