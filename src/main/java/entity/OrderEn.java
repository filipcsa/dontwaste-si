package entity;


import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity
@NamedQueries({
        @NamedQuery(name = "findAllActiveOtherOrders",
                query = "select o from OrderEn o where o.active = true and NOT o.endUser.username = :username"),
        @NamedQuery(name = "findAllActiveMyOrders",
                query = "select o from OrderEn o where o.active = true and o.endUser.username = :username")
})
public class OrderEn implements Serializable {

    @Id
    @GeneratedValue
    protected Long id;

    @NotNull
    private boolean active;

    @NotNull
    @Size(min = 3, max = 40)
    private String name;

    @NotNull
    @Size(min = 5, max = 1000)
    private String description;

    @NotNull
    @Size(min = 1)
    private String address;

    @ManyToOne
    @JoinColumn(name="enduser_id", referencedColumnName="id", nullable=false)
    private EndUser endUser;

    public boolean isActive() { return active; }

    public void setActive(boolean active) { this.active = active; }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public Long getId() { return id; }

    public void setId(Long id) { this.id = id; }

    public String getDescription() { return description; }

    public void setDescription(String description) { this.description = description; }

    public String getAddress() { return address; }

    public void setAddress(String address) { this.address = address; }

    public EndUser getEndUser() {
        return this.endUser;
    }

    public void setEndUser(EndUser endUser) {
        this.endUser = endUser;
    }
}
