package entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@NamedQueries({
        @NamedQuery(name = "findAccountByUsername",
                query = "select a from Account a where a.username = :name")
})
public class Account implements Serializable {

    @Id
    @GeneratedValue
    protected Long id;

    @NotNull
    @Column(unique=true)
    @Size(min = 4, max = 25)
    protected String username;

    @NotNull
    protected String password;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
