package dao;

import entity.Account;

import javax.ejb.Stateless;
import javax.enterprise.context.Dependent;
import javax.persistence.TypedQuery;

@Dependent
public class AccountDAOImpl extends GenericDAO<Account> implements AccountDAO{

    @Override
    public Account findAccountByUsername(String username) {
        TypedQuery<Account> query = em
                .createNamedQuery("findAccountByUsername", Account.class)
                .setParameter("name", username);
        return query.getSingleResult();
    }
}
