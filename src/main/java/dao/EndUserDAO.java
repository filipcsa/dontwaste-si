package dao;

import entity.Account;
import entity.EndUser;
import entity.OrderEn;

import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Set;

public interface EndUserDAO extends DAO<EndUser> {

    List<OrderEn> findAllUserOrders(String username);
    EndUser findEndUserByUsername(String username);
}
