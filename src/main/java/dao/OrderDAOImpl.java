package dao;

import entity.OrderEn;

import javax.enterprise.context.Dependent;
import javax.persistence.TypedQuery;
import java.util.List;

@Dependent
public class OrderDAOImpl extends GenericDAO<OrderEn> implements OrderDAO {

    @Override
    public List<OrderEn> findAllActiveOtherOrders(String username) {
        TypedQuery<OrderEn> query = em
                .createNamedQuery("findAllActiveOtherOrders", OrderEn.class)
                .setParameter("username", username);
        return query.getResultList();
    }

    @Override
    public List<OrderEn> findAllActiveMyOrders(String username) {
        TypedQuery<OrderEn> query = em
                .createNamedQuery("findAllActiveMyOrders", OrderEn.class)
                .setParameter("username", username);
        return query.getResultList();
    }

}
