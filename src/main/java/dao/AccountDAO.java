package dao;

import entity.Account;

public interface AccountDAO extends DAO<Account>{

    Account findAccountByUsername(String username);
}
