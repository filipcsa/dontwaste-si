package dao;

import entity.Account;
import entity.EndUser;
import entity.OrderEn;

import javax.enterprise.context.Dependent;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Dependent
public class EndUserDAOImpl extends GenericDAO<EndUser> implements EndUserDAO {

    @Override
    public List<OrderEn> findAllUserOrders(String username) {
        TypedQuery<EndUser> query = em
                .createNamedQuery("findEndUserByUsername", EndUser.class)
                .setParameter("username", username);
        EndUser en = query.getSingleResult();
        return new ArrayList<>(en.getOrders());
    }

    @Override
    public EndUser findEndUserByUsername(String username) {
        TypedQuery<EndUser> query = em
                .createNamedQuery("findEndUserByUsername", EndUser.class)
                .setParameter("username", username);
        return query.getSingleResult();
    }

}
