package dao;

import entity.OrderEn;

import java.util.List;

public interface OrderDAO extends DAO<OrderEn> {

    List<OrderEn> findAllActiveOtherOrders(String username);
    List<OrderEn> findAllActiveMyOrders(String username);

}
