INSERT INTO Account ("id", "username", "password") VALUES
(69, 'Csaba', '$2a$04$8RxhkE3VGLXC.EvzHvAEBOCmzRun93We/4JjkzS.fCC8daXU7Z4IO'),
(70, 'Martin', '$2a$04$mRs3Z5VstLLX9Lao4L02pu1TUgY2HtWquUkZd.5hwMUk7hAzUzD4S');


INSERT INTO EndUser ("id", "username", "password", "firtsname", "lastname", "email") VALUES
(50, 'user1', '$2a$04$mRs3Z5VstLLX9Lao4L02pu1TUgY2HtWquUkZd.5hwMUk7hAzUzD4S', 'Pilif', 'Abasc', 'cfilip@gmail.com'),
(51, 'user2', '$2a$04$mRs3Z5VstLLX9Lao4L02pu1TUgY2HtWquUkZd.5hwMUk7hAzUzD4S', 'Nitram', 'Anyrut', 'mturyna@gmail.com');

INSERT INTO OrderEn ("id", "active", "name", "description", "address", "enduser_id") VALUES
(66, TRUE , 'Half-drinked cola', 'Awesome half-drinked cola', 'Karlovo Namesti, Werneruv kabinet', 50),
(67, TRUE , 'Bablbam', 'Very good bablbam for reuse', 'Dlouha 10, Zelenec', 50),
(68, TRUE , 'Roasted chicken', 'I roasted chicken 10 days ago and even dogs wont eat it', 'Kratka 69, Praha', 50),
(69, TRUE , 'White yogurt', 'I dont eat those healthy shits', 'Praha', 51),
(70, TRUE , 'Oreo cookie', 'Ouch, veeery goood cookie', 'Praha', 51);